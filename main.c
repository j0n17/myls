/*
** main.c for test in /home/maire_j/Desktop/test
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 31 10:58:10 2013 Maire Jonathan
** Last update Fri Nov  1 09:10:15 2013 Maire Jonathan
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <my.h>
#include "stats.h"

int	main(int argc, char **argv)
{
  my_put_nbr(my_get_size(argv[1]));
  my_putchar('\n');

  my_put_nbr(my_get_inode(argv[1]));
  my_putchar('\n');

  my_get_guid(argv[1], 0);
  my_putchar('\n');

  my_put_nbr(my_get_guid(argv[1], 1));
  my_putchar('\n');

  my_get_mtime(argv[1]);
  return (0);
}
