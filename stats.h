/*
** stats.h for stats.h in /home/maire_j/Desktop/myls
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 31 17:19:02 2013 Maire Jonathan
** Last update Fri Nov  1 09:00:59 2013 Maire Jonathan
*/

#ifndef STATS_H_
# define STATS_H_

int	my_get_size(char *);
int	my_get_inode(char *);
int	my_get_guid(char *, int);
void	my_get_mtime(char *);

#endif /* STATS_H_ */
