##
## Makefile for Makefile in /home/maire_j/rendu/myls-2013-maire_j
## 
## Made by Maire Jonathan
## Login   <maire_j@epitech.net>
## 
## Started on  Mon Oct 28 10:27:34 2013 Maire Jonathan
## Last update Fri Nov  1 08:54:48 2013 Maire Jonathan
##

CC	= gcc

RM	= rm -f

NAME	= myls

SRCS	= main.c \
	  my_get_stat.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(NAME) -lmy
	$(RM) $(OBJS)
clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all
